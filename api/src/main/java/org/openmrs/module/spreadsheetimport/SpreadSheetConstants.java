package org.openmrs.module.spreadsheetimport;

/**
 * This class holds constants used in more than one class in the SPREADSHEET module.
 * 
 * @author Daniel
 *
 */
public class SpreadSheetConstants {
	//TODO More constants need to be put in this class from the various classes where they are scattered.
	/** The form designer key for the decimal separators.*/
	public static final String FORM_DESIGNER_KEY_DECIMAL_SEPARATORS = "decimalSeparators";
	
	/** The global property key for the date submit format.*/
	public static final String GLOBAL_PROP_KEY_DATE_SUBMIT_FORMAT = "spreadsheetimport.dateSubmitFormat";
	
	/** The global property key for the datetime submit format.*/
	public static final String GLOBAL_PROP_KEY_DATE_TIME_SUBMIT_FORMAT = "spreadsheetimport.dateTimeSubmitFormat";
	
	/** The global property key for the time submit format.*/
	public static final String GLOBAL_PROP_KEY_TIME_SUBMIT_FORMAT = "spreadsheetimport.timeSubmitFormat";
	
	/** The global property key for the date display format.*/
	public static final String GLOBAL_PROP_KEY_DATE_DISPLAY_FORMAT = "spreadsheetimport.dateDisplayFormat";
	
	/** The global property key for the datetime display format.*/
	public static final String GLOBAL_PROP_KEY_DATE_TIME_DISPLAY_FORMAT = "spreadsheetimport.dateTimeDisplayFormat";
	
	/** The global property key for the time display format.*/
	public static final String GLOBAL_PROP_KEY_TIME_DISPLAY_FORMAT = "spreadsheetimport.timeDisplayFormat";
	
	/** The global property key for the time display format.*/
	public static final String GLOBAL_PROP_KEY_SHOW_SUBMIT_SUCCESS_MSG = "spreadsheetimport.showSubmitSuccessMsg";
	
	/** The global property key for the decimal separators.*/
	public static final String GLOBAL_PROP_KEY_DECIMAL_SEPARATORS = "spreadsheetimport.decimalSeparators";
	
	/** The global property key for the current locale.*/
	public static final String GLOBAL_PROP_KEY_LOCALE = "spreadsheetimport.locale";
	
	/** The global property key for the default font family.*/
	public static final String GLOBAL_PROP_KEY_DEFAULT_FONT_FAMILY = "spreadsheetimport.defaultFontFamily";
	
	/** The global property key for the default font size.*/
	public static final String GLOBAL_PROP_KEY_DEFAULT_FONT_SIZE = "spreadsheetimport.defaultFontSize";
	
	/** The global property key for the list of locales.*/
	public static final String GLOBAL_PROP_KEY_LOCALE_LIST = "spreadsheetimport.localeList";
	
	
	/** The default character encoding used when writting and reading bytes to and from streams. */
	public static final String DEFAULT_CHARACTER_ENCODING = "UTF-8";
	
	/** The default submit date format. */
	public static final String DEFAULT_DATE_SUBMIT_FORMAT = "yyyy-MM-dd";//yyyy-mm-dd
	
	/** The default submit datetime format. */
	public static final String DEFAULT_DATE_TIME_SUBMIT_FORMAT = "yyyy-MM-dd hh:mm a";
	
	/** The default submit time format. */
	public static final String DEFAULT_TIME_SUBMIT_FORMAT = "hh:mm a";
	
	/** The default display date format. */
	public static final String DEFAULT_DATE_DISPLAY_FORMAT = "dd-MM-yyyy";//yyyy-mm-dd
	
	/** The default display date format. */
	public static final String DEFAULT_DATE_TIME_DISPLAY_FORMAT = "dd-MM-yyyy hh:mm a";
	
	/** The default display date format. */
	public static final String DEFAULT_TIME_DISPLAY_FORMAT = "hh:mm a";
	
	/** The default flag to determine whether to display the submit success message or not. */
	public static final String DEFAULT_SHOW_SUBMIT_SUCCESS_MSG = "false";
	
	/** The default value for the decimal separators. */
	public static final String DEFAULT_DECIMAL_SEPARATORS = "en:.;fr:.;es:,;it:.;pt:.";
	
	/** The default font family. */
	public static final String DEFAULT_FONT_FAMILY = "Verdana, 'Lucida Grande', 'Trebuchet MS', Arial, Sans-Serif";
		
	/** The default font size. */
	public static final String DEFAULT_FONT_SIZE = "16";
		
	/** The default locale list. */
	public static final String DEFAULT_LOCALE_LIST = "en:English,fr:French,gr:German,swa:Swahili";
}
	