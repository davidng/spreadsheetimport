package org.openmrs.module.spreadsheetimport.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.GlobalProperty;
import org.openmrs.api.AdministrationService;
import org.openmrs.api.context.Context;
import org.openmrs.util.OpenmrsUtil;
import org.openmrs.web.WebConstants;
import org.openmrs.module.spreadsheetimport.SpreadSheetConstants;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Controller for properties page jsp
 * not use, should use advance settings.
 * @author Samuel Mbugua
 */
@Controller
@Deprecated
public class SpreadsheetImportPropertiesController {
	
	public static final String PROP_NAME = "property";
	
	public static final String PROP_VAL_NAME = "value";
	
	public static final String PROP_DESC_NAME = "description";
	
	/** Logger for this class and subclasses */
	protected final Log log = LogFactory.getLog(getClass());
	
	@RequestMapping(value="/module/spreadsheetimport/properties", method=RequestMethod.GET)
	public Map<String, Object> populateForm() {
		AdministrationService as = Context.getAdministrationService();
		
		//Here we return only user configurable properties
		Map<String, Object> map =new HashMap<String, Object>();
		List<GlobalProperty> localProps = as.getGlobalPropertiesByPrefix("spreadsheetimport.");
		
		map.put("globalProps", localProps);
		map.put(SpreadSheetConstants.FORM_DESIGNER_KEY_DECIMAL_SEPARATORS, Context.getAdministrationService().getGlobalProperty(SpreadSheetConstants.GLOBAL_PROP_KEY_DECIMAL_SEPARATORS, SpreadSheetConstants.DEFAULT_DECIMAL_SEPARATORS));
		return map;
	}
	
	@RequestMapping(value="/module/spreadsheetimport/properties", method=RequestMethod.POST)
	protected String saveProperties(HttpServletRequest request) {
		
		String action = request.getParameter("action");
		if (action == null)
			action = "cancel";
		
		if (action.equals(Context.getMessageSourceService().getMessage("general.save"))) {
			HttpSession httpSession = request.getSession();
			
			if (Context.isAuthenticated()) {
				AdministrationService as = Context.getAdministrationService();
				
				//fetch all properties and save it to a hashmap for easy retrieval of already-used-GPs
				List<GlobalProperty> properties = as.getAllGlobalProperties();
				Map<String, GlobalProperty> propertiesMap = new HashMap<String, GlobalProperty>();
				for (GlobalProperty prop : properties) {
					propertiesMap.put(prop.getProperty(), prop);
				}
				
				// the list we'll save to the database
				List<GlobalProperty> globalPropList = properties;
				
				String[] keys = request.getParameterValues(PROP_NAME);
				String[] values = request.getParameterValues(PROP_VAL_NAME);
				String[] descriptions = request.getParameterValues(PROP_DESC_NAME);
				
				for (int x = 0; x < keys.length; x++) {
					String key = keys[x];
					String val = values[x];
					String desc = descriptions[x];
					
					// try to get an already-used global property for this key
					GlobalProperty tmpGlobalProperty = propertiesMap.get(key);
					
					// if it exists, use that object...just update it
					if (tmpGlobalProperty != null) {
						tmpGlobalProperty.setPropertyValue(val);
						tmpGlobalProperty.setDescription(desc);
						globalPropList.add(tmpGlobalProperty);
					} else {
						// if it doesn't exist, create a new global property
						globalPropList.add(new GlobalProperty(key, val, desc));
					}
				}
				try {
					as.saveGlobalProperties(globalPropList);
					httpSession.setAttribute(WebConstants.OPENMRS_MSG_ATTR, "GlobalProperty.saved");
					
					// refresh log level from global property(ies)
					OpenmrsUtil.applyLogLevels();
				}
				catch (Exception e) {
					log.error("Error saving properties", e);
					httpSession.setAttribute(WebConstants.OPENMRS_MSG_ATTR, "GlobalProperty.not.saved");
					httpSession.setAttribute(WebConstants.OPENMRS_MSG_ATTR, e.getMessage());
				}
			}
		}
		return "redirect:properties.list";
	}

}
