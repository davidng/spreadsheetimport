<ul id="menu">
	<li class="first">
		<a href="${pageContext.request.contextPath}/admin"><openmrs:message code="admin.title.short"/></a>
	</li>
	<openmrs:hasPrivilege privilege="Save Spreadsheet Import Template">
		<li <c:if test='<%= request.getRequestURI().contains("spreadsheetimportForm") %>'>class="active"</c:if>>
			<a href="spreadsheetimport.form">
				<openmrs:message code="spreadsheetimport.new_import_template"/>
			</a>
		</li>
	</openmrs:hasPrivilege>
	<li <c:if test='<%= request.getRequestURI().contains("spreadsheetimportTemplateList") %>'>class="active"</c:if>>
		<a href="spreadsheetimport.list">
			<openmrs:message code="spreadsheetimport.import_template"/>
		</a>
	</li>
</ul>