<%--
  The contents of this file are subject to the OpenMRS Public License
  Version 1.0 (the "License"); you may not use this file except in
  compliance with the License. You may obtain a copy of the License at
  http://license.openmrs.org

  Software distributed under the License is distributed on an "AS IS"
  basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
  License for the specific language governing rights and limitations
  under the License.

  Copyright (C) OpenMRS, LLC.  All Rights Reserved.

--%>
<%@ include file="/WEB-INF/template/include.jsp" %>
<openmrs:require privilege="List Spreadsheet Import Templates" otherwise="/login.htm" redirect="/module/spreadsheetimport/spreadsheetimport.list"/>
<%@ include file="/WEB-INF/template/header.jsp" %>
<%@ include file="localHeader.jsp" %>

<h2><openmrs:message code=""/><openmrs:message code="spreadsheetimport.import_template_list"/></h2>
<openmrs:hasPrivilege privilege="Save Spreadsheet Import Template">
	<a href="spreadsheetimport.form"><openmrs:message code=""/><openmrs:message code="spreadsheetimport.new"/></a>
</openmrs:hasPrivilege>
<p />

<c:if test="${fn:length(templates) > 0}">
	<form method="post">
	   	<table cellpadding="2">
        	<tr>
        		<th>&nbsp;</th>
            	<th><openmrs:message code="spreadsheetimport.msg.name2"/></th>
            	<th><openmrs:message code="spreadsheetimport.msg.description2"/></th>
            	<th><openmrs:message code="spreadsheetimport.action"/></th>
        	</tr>
        	<c:forEach var="template" items="${templates}">
        	   	<tr>
               		<td><input type="checkbox" name="${template.id}"/></td>
               		<td><a href="spreadsheetimport.form?id=${template.id}">${template.name}</a></td>
                   	<td>${template.description}</td>
                   	<td><openmrs:hasPrivilege privilege="Save Spreadsheet Import Template">
                   		<a href="spreadsheetimportImport.form?id=${template.id}"><openmrs:message code="spreadsheetimport.import"/></a>
                   		</openmrs:hasPrivilege>
                   	</td>
               	</tr>
        	</c:forEach>          
    	</table>
    	<p />
		<td><input type="submit" value='<openmrs:message code="spreadsheetimport.btn.delete"/>'/></td>
	</form>
</c:if>

<%@ include file="/WEB-INF/template/footer.jsp" %>