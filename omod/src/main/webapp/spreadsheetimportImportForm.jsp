<%--
  The contents of this file are subject to the OpenMRS Public License
  Version 1.0 (the "License"); you may not use this file except in
  compliance with the License. You may obtain a copy of the License at
  http://license.openmrs.org

  Software distributed under the License is distributed on an "AS IS"
  basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
  License for the specific language governing rights and limitations
  under the License.

  Copyright (C) OpenMRS, LLC.  All Rights Reserved.

--%>
<%@ include file="/WEB-INF/template/include.jsp" %>
<openmrs:require anyPrivilege="Save Spreadsheet Import Template, Import Spreadsheet Import Template" otherwise="/login.htm" redirect="/module/spreadsheetimport/spreadsheetimportImport.form"/>
<%@ include file="/WEB-INF/template/header.jsp" %>
<%@ include file="localHeader.jsp" %>
<%@ page session="true" %>
<%@ page pageEncoding="UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<form method="post" enctype="multipart/form-data">
    <form:errors path="*" cssClass="error"/>
	<input type="hidden" name="id" value="${template.id}"/> <br/>
	<b>${template.name}</b><br />
	<openmrs:message code="spreadsheetimport.msg.spreadsheet_to_upload"/><input type="file" name="file" /> <br/>
<!-- put value = "" because if it's empty and the user doesn't enter anything it uses the first sheet -->
	<openmrs:message code="spreadsheetimport.msg.sheet"/><input type="text" name="sheet" value=""/> <br/>
<!-- BEGIN: FOR TESTING ONLY 
	<input type="checkbox" name="rollbackTransaction"/><openmrs:message code="spreadsheetimport.msg.rollback_transaction"/> <br/>
 END: FOR TESTING ONLY -->
	<input type="submit" value='<openmrs:message code="spreadsheetimport.btn.upload"/>'/>
</form>

<%@ include file="/WEB-INF/template/footer.jsp"%>
